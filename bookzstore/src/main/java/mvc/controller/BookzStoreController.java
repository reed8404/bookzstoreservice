package mvc.controller;

import mvc.model.book.Book;
import mvc.model.book.BookList;
import mvc.service.book.BookListDAO;
import mvc.service.book.BookListDAOImpl;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Reed on 7/30/2015.
 */

@RestController
public class BookzStoreController {

    BookListDAO bookListDAO = BookListDAOImpl();

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public BookList getAllBooks() {

        BookListDAO bookListDao = new BookListDAOImpl();
        BookList books = null;
        books = bookListDao.getBooks();

        return books;
    }

    @RequestMapping(value = "/books/author/{author}", method = RequestMethod.GET)
    public BookList getBooksByAuthor(@PathVariable String author) {

        BookListDAO bookListDao = new BookListDAOImpl();
        BookList books = null;
        books = bookListDao.getBooksByAuthor(author);

        return books;
    }

    @RequestMapping(value = "/submitBook", method = RequestMethod.POST)
    public void setNewBook(@RequestBody Book book) {

        bookListDao.submitBook(book);

    }


}
