package mvc.model.customer;

/**
 * Created by Reed on 7/30/2015.
 */
public class Customer {


/////
//  Customer Fields:    customerId, customerFirstName, customerLastName,
//                      customerEmail, customerUserName, customerPassword, customerAge
/////

    private int customerId;
    private String customerFirstName;
    private String customerLastName;
    private String customerEmail;
    private String customerUserName;
    private String customerPassword;
    private int customerAge;

/////
//  Customer constructors:
/////

    public Customer(
                    int customerId, String customerFirstName,
                    String customerLastName, String customerEmail,
                    String customerUserName, String customerPassword,
                    int customerAge
                    ) {

        this.customerId = customerId;
        this.customerFirstName = customerFirstName;
        this.customerLastName = customerLastName;
        this.customerEmail = customerEmail;
        this.customerUserName = customerUserName;
        this.customerPassword = customerPassword;
        this.customerAge = customerAge;
    }

/////
//  customerId Methods: getCustomerId, setCustomerId
/////

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

/////
//  customerFirstName Methods: getCustomerFirstName, setCustomerFirstName
/////

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

/////
//  customerLastName Methods: getCustomerLastName, setCustomerLastName
/////

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

/////
//  customerEmail Methods: getCustomerEmail, setCustomerEmail
/////

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

/////
//  customerUserName Methods: getCustomerUserName, setCustomerUserName
/////

    public String getCustomerUserName() {
        return customerUserName;
    }

    public void setCustomerUserName(String customerUserName) {
        this.customerUserName = customerUserName;
    }

/////
//  customerEmail Methods: getCustomerEmail, setCustomerEmail
/////

    public String getCustomerPassword() {
        return customerPassword;
    }

    public void setCustomerPassword(String customerPassword) {
        this.customerPassword = customerPassword;
    }

/////
//  customerAge Methods: getCustomerAge, setCustomerAge
/////

    public int getCustomerAge() {
        return customerAge;
    }

    public void setCustomerAge(int customerAge) {
        this.customerAge = customerAge;
    }

}
