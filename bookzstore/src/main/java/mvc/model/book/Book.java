package mvc.model.book;

/**
 * Created by Reed on 7/30/2015.
 */
public class Book {

/////
//  Customer Fields:    bookISBN, bookTitle, bookAuthor, bookPublisherName,
//                      bookPublicationYear, bookBinding, bookSourceNumber,
//                      bookRetailPrice, bookInventoryQuantity
/////

    private String bookISBN;
    private String bookTitle;
    private String bookAuthor;
    private String bookPublisherName;
    private String bookPublicationYear;
    private String bookBinding;
    private String bookSourceNumber;
    private String bookRetailPrice;
    private String bookInventoryQuantity;

/////
//  Book constructors:
/////

    public Book(
                    String bookISBN, String bookTitle, String author,
                    String bookPublisherName, String bookPublicationYear,
                    String bookBinding, String bookSourceNumber, String bookRetailPrice,
                    String bookInventoryQuantity
                ) {

        this.bookISBN = bookISBN;
        this.bookTitle = bookTitle;
        this.bookAuthor = author;
        this.bookPublisherName = bookPublisherName;
        this.bookPublicationYear = bookPublicationYear;
        this.bookBinding = bookBinding;
        this.bookSourceNumber = bookSourceNumber;
        this.bookRetailPrice = bookRetailPrice;
        this.bookInventoryQuantity = bookInventoryQuantity;
    }

/////
//  BookISBN Methods: getBookISBN, setBookISBN
/////

    public String getBookISBN() {
        return bookISBN;
    }

    public void setBookISBN(String bookISBN) {
        this.bookISBN = bookISBN;
    }

/////
//  BookTitle Methods: getBookTitle, setBookTitle
/////

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

/////
//  BookAuthor Methods:  getBookAuthor,  setBookAuthor
/////

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

/////
//  BookPublisherName Methods: getCustomerId, setCustomerId
/////

    public String getBookPublisherName() {
        return bookPublisherName;
    }

    public void setBookPublisherName(String bookPublisherName) {
        this.bookPublisherName = bookPublisherName;
    }

/////
//  BookPublicationYear Methods: getCustomerId, setCustomerId
/////

    public String getBookPublicationYear() {
        return bookPublicationYear;
    }

    public void setBookPublicationYear(String bookPublicationYear) {
        this.bookPublicationYear = bookPublicationYear;
    }

/////
//  BookBinding Methods: getCustomerId, setCustomerId
/////

    public String getBookBinding() {
        return bookBinding;
    }

    public void setBookBinding(String bookBinding) {
        this.bookBinding = bookBinding;
    }

/////
//  BookSourceNumber Methods: getCustomerId, setCustomerId
/////

    public String getBookSourceNumber() {
        return bookSourceNumber;
    }

    public void setBookSourceNumber(String bookSourceNumber) {
        this.bookSourceNumber = bookSourceNumber;
    }

/////
//  BookRetailPrice Methods: getCustomerId, setCustomerId
/////

    public String getBookRetailPrice() {
        return bookRetailPrice;
    }

    public void setBookRetailPrice(String bookRetailPrice) {
        this.bookRetailPrice = bookRetailPrice;
    }

/////
//  BookInventoryQuantity Methods: getCustomerId, setCustomerId
/////

    public String getBookInventoryQuantity() {
        return bookInventoryQuantity;
    }

    public void setBookInventoryQuantity(String bookInventoryQuantity) {
        this.bookInventoryQuantity = bookInventoryQuantity;
    }
}
