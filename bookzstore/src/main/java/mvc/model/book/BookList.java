package mvc.model.book;

/**
 * Created by Reed on 7/30/2015.
 */

import mvc.model.book.Book;

import java.util.ArrayList;
import java.util.List;

public class BookList {

    private List<Book> books;

    public List<Book> getBooks() {
        if (books == null)
            books = new ArrayList<Book>();
        return books;
    }
}
