package mvc.viewresolver.excel;

import mvc.model.book.BookList;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.Map;

/**
 * Created by Reed on 7/31/2015.
 */

public class ExcelView extends AbstractExcelView {
    @Override
    protected void buildExcelDocument(
                                        Map<String, Object> model,
                                        HSSFWorkbook workbook, HttpServletRequest request,
                                        HttpServletResponse response
                                        ) throws Exception {

        BookList bookList = (BookList) model.get("bookList");

        Sheet sheet = workbook.createSheet("sheet 1");

        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        Row row = null;
        Cell cell = null;
        int rowCount = 0;
        int colCount = 0;

        // Create header cells
        row = sheet.createRow(rowCount++);

        cell = row.createCell(colCount++);
        cell.setCellStyle(style);
        cell.setCellValue("Title");

        cell = row.createCell(colCount++);
        cell.setCellStyle(style);
        cell.setCellValue("Author");

        // Create data cells
        row = sheet.createRow(rowCount++);
        colCount = 0;

        private static void titleCellAdder() {

            StringBuffer titleStringBuffers = new StringBuffer("");

            for (String titleStringBuffer : book.getTitle()) {
                titleStringBuffers.append(titleStringBuffer);
                titleStringBuffers.append(" ");
            }

            row.createCell(colCount++).setCellValue(titleStringBuffers.toString());

            for (int i = 0; i < 3; i++) {
                sheet.autoSizeColumn(i, true);
            }
        }

        private static void authorCellAdder() {

            StringBuffer authorStringBuffers = new StringBuffer("");

            for (String authorStringBuffer : book.getTitle()) {
                authorStringBuffers.append(authorStringBuffer);
                authorStringBuffers.append(" ");
            }

            row.createCell(colCount++).setCellValue(authorStringBuffers.toString());

            for (int i = 0; i < 3; i++) {
                sheet.autoSizeColumn(i, true);
            }

        }

    }
}
