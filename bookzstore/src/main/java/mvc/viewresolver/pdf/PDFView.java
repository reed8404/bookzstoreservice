package mvc.viewresolver.pdf;

import mvc.model.book.BookList;
import org.springframework.web.servlet.view.document.AbstractPdfView;
import mvc.model.book.BookList;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import java.awt.*;
import java.util.Map;

/**
 * Created by Reed on 7/31/2015.
 */
public class PDFView extends AbstractPdfView {
    @Override
    protected void buildPdfDocument(
                                    Map<String, Object> model,
                                    Document document, PdfWriter writer, HttpServletRequest request,
                                    HttpServletResponse response
                                    ) throws Exception {

        BookList bookList = (BookList) model.get("bookList");

        PdfPTable table = new PdfPTable(2);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.getDefaultCell().setBackgroundColor(Color.lightGray);

        table.addCell("Title");
        table.addCell("Author");

        private static void titleCellAdder() {

            StringBuffer titleStringBuffers = new StringBuffer("");

            for (String titleStringBuffer : book.getTitle()) {
                titleStringBuffers.append(titleStringBuffer);
                titleStringBuffers.append(" ");
            }

            table.addCell(titleStringBuffers.toString());
            document.add(table);
        }

        private static void authorCellAdder() {

            StringBuffer authorStringBuffers = new StringBuffer("");

            for (String authorStringBuffer : book.getTitle()) {
                authorStringBuffers.append(authorStringBuffer);
                authorStringBuffers.append(" ");
            }

            table.addCell(authorStringBuffers.toString());
            document.add(table);

        }

    }
}
