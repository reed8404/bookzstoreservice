package mvc.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by Reed on 7/30/2015.
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.craigreedwilliams.mvc")
public class RESTConfiguration {
}
