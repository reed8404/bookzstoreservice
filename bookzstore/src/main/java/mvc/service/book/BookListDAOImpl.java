package mvc.service.book;

import mvc.model.book.Book;
import mvc.model.book.BookList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by Reed on 7/31/2015.
 */

public class BookListDAOImpl implements BookListDAO {

    private Connection connection;
    private Statement statement;

    public BookListDAOImpl() {
    }

    public BookList getBooksByAuthor(String author) {

        String query = "SELECT * FROM employee WHERE author=" + author;
        ResultSet rs = null;
        BookList books = new BookList();

        try {
            // Connect to SQL database
            // retrieve and convert result set into BookList data

            // For testing we'll put in a fake book list
            Book book1 = new Book(bookISBN, "Christmas Carol", "Charles Dickens", bookPublisherName, bookPublicationYear, bookBinding, bookSourceNumber, bookRetailPrice, bookInventoryQuantity);
            Book book2 = new Book(bookISBN, "Tale of Two Cities", "Charles Dickens", bookPublisherName, bookPublicationYear, bookBinding, bookSourceNumber, bookRetailPrice, bookInventoryQuantity);
            books.getBooks().add(book1);
            books.getBooks().add(book2);
        } finally {
            // close all connections/statements/etc.
        }

        return books;
    }

    public BookList getBooks() {
        String query = "SELECT * FROM books";
        ResultSet rs = null;
        BookList books = new BookList();

        try {
            // Connect to SQL database
            // retrieve and convert result set into BookList data

            // For testing we'll put in a fake book list
            Book book1 = new Book(bookISBN, "Christmas Carol", "Charles Dickens", bookPublisherName, bookPublicationYear, bookBinding, bookSourceNumber, bookRetailPrice, bookInventoryQuantity);
            Book book2 = new Book(bookISBN, "Hop on Pop", "Dr. Seuss", bookPublisherName, bookPublicationYear, bookBinding, bookSourceNumber, bookRetailPrice, bookInventoryQuantity);
            books.getBooks().add(book1);
            books.getBooks().add(book2);

        } finally {
            // close all connections/statements/etc.
        }

        return books;
    }

    public void submitBook(Book book) {

        String query = "SELECT * FROM books";
        ResultSet rs = null;
        BookList books = null;

        try {
            // Connect to SQL database
            // retrieve and convert result set into BookList data
        } finally {
            // close all connections/statements/etc.
        }

    }

}
