package mvc.service.book;

import mvc.model.book.Book;
import mvc.model.book.BookList;

/**
 * Created by Reed on 7/31/2015.
 */
public interface BookListDAO {

    public BookList getBooksByAuthor(String author);

    public BookList getBooks();

    public void submitBook(Book book);

}
