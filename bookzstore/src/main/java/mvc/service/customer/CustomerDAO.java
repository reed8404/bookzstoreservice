package mvc.service.customer;

import mvc.model.customer.Customer;

/**
 * Created by Reed on 7/31/2015.
 */
public interface CustomerDAO {
    public void insert(Customer customer);

    public Customer findByCustomerId(int customerId);
}
